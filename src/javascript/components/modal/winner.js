import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function
  const { name, source } = fighter;
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const image = createFighterImage(fighter);
  bodyElement.appendChild(image);
  showModal({ title: `${name} is the winner!`, bodyElement });
}
