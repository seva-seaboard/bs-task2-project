import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  // fighterElement.append(createFighterImage(fighter));
  if (fighter) {
    const imageElement = createFighterImage(fighter);
    const { dataName, dataBlock } = createFighterData(fighter);
    fighterElement.append(dataName, imageElement, dataBlock);
  }
  return fighterElement;
}

function createFighterData(fighter) {
  if (fighter) {
    const { name, health, attack, defense } = fighter;
    const dataName = createElement({ tagName: 'h2', className: 'name__preview' });
    const dataBlock = createElement({ tagName: 'div', className: 'data__preview' });
    const dataHealth = createElement({ tagName: 'p', className: 'health__preview' });
    const dataAttack = createElement({ tagName: 'p', className: 'attack__preview' });
    const dataDefense = createElement({ tagName: 'p', className: 'defense__preview' });
    dataName.innerText = name;
    dataHealth.innerText = `Health: ${health}`;
    dataAttack.innerText = `Attack: ${attack}`;
    dataDefense.innerText = `Defense: ${defense}`;
    dataBlock.append(dataHealth, dataAttack, dataDefense);
    return {
      dataName,
      dataBlock,
    };
  }
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
