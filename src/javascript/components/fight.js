import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const first = { ...firstFighter, block: false, criticalHit: true };
    const second = { ...secondFighter, block: false, criticalHit: true };
    const {
      PlayerOneAttack,
      PlayerOneBlock,
      PlayerTwoAttack,
      PlayerTwoBlock,
      PlayerOneCriticalHitCombination,
      PlayerTwoCriticalHitCombination,
    } = controls;

    let indicatorFirst = document.querySelector('#left-fighter-indicator');
    let indicatorSecond = document.querySelector('#right-fighter-indicator');

    let pressedKeysFirst = [];
    let pressedKeysSecond = [];
    let arrSet = new Set();

    const handleKeyDown = (e) => {
      if (e.code === PlayerOneAttack && !second.block && !first.block) {
        const damage = getDamage(firstFighter, secondFighter);
        if (second.health >= 0) {
          second.health -= damage;
        }
      }

      if (e.code === PlayerTwoAttack && !first.block && !second.block) {
        const damage = getDamage(secondFighter, firstFighter);
        if (first.health >= 0) {
          first.health -= damage;
        }
      }
      if (PlayerOneCriticalHitCombination.includes(e.code)) {
        pressedKeysFirst = [...pressedKeysFirst, e.code];
        arrSet = [...new Set(pressedKeysFirst)];
        if (arrSet.length === PlayerOneCriticalHitCombination.length) {
          if (first.criticalHit && second.health >= 0 && !first.block) {
            const damage = criticalHitPower(firstFighter);
            second.health -= damage;
            first.criticalHit = false;
            setTimeout(() => (first.criticalHit = true), 10000);
          }
        }
      }
      if (PlayerTwoCriticalHitCombination.includes(e.code)) {
        pressedKeysSecond = [...pressedKeysSecond, e.code];
        arrSet = [...new Set(pressedKeysSecond)];
        if (arrSet.length === PlayerTwoCriticalHitCombination.length) {
          if (second.criticalHit && first.health >= 0 && !second.block) {
            const damage = criticalHitPower(secondFighter);
            first.health -= damage;
            second.criticalHit = false;
            setTimeout(() => (second.criticalHit = true), 10000);
          }
        }
      }

      if (e.code === PlayerOneBlock) first.block = true;
      if (e.code === PlayerTwoBlock) second.block = true;
    };
    const handleKeyUp = (e) => {
      if (e.code === PlayerOneBlock) first.block = false;
      if (e.code === PlayerTwoBlock) second.block = false;
      if (arrSet.length > 0) {
        arrSet.clear;
      }
      const end = first.health <= 0 || second.health <= 0;
      const fighter = first.health <= 0 ? secondFighter : firstFighter;
      const secondIndicatorWidth = (second.health / secondFighter.health) * 100;
      const firstIndicatorWidth = (first.health / firstFighter.health) * 100;
      if (firstIndicatorWidth <= 40) {
        indicatorFirst.style.background = 'red';
      }
      if (secondIndicatorWidth <= 40) {
        indicatorSecond.style.background = 'red';
      }
      indicatorSecond.style.width = `${secondIndicatorWidth}%`;
      indicatorFirst.style.width = `${firstIndicatorWidth}%`;
      if (end) {
        resolve({ message: 'Finished', fighter });
      }
    };

    window.addEventListener('keydown', handleKeyDown);
    window.addEventListener('keyup', handleKeyUp);
  });
}

function criticalHitPower(fighter, keys) {
  return fighter.attack * 2;
}

export function getDamage(attacker, defender) {
  // return damage
  if (getBlockPower(defender) > getHitPower(attacker)) return 0;
  return getHitPower(attacker) - getBlockPower(defender);
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() * 1 + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() * 1 + 1;
  return fighter.defense * dodgeChance;
}
